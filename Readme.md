# REDMINE API

[![N|Solid](http://www.buenosaires.gob.ar/sites/gcaba/themes/gcbaV4/xgcaba/css/BA2016.png)](http://www.buenosaires.gob.ar)

Redmine API es un script interno para la carga de http://desa.gcba.gob.ar/redmine

### Pasos:

#### 1.
    Ir a: http://desa.gcba.gob.ar/redmine/my/account y crear la key para la API
```sh
$ cp config/config.default.json config/config.json
```
    Modificar el config/config.json y agregar la key de redmine

#### 2.
```sh
$ npm install
$ node index.js tareaspadre #Para cargar las tareas padre del los proyectos (requerimiento) que tenga asignado el usuario. Es importante correrlo varias veces por problemas en la red.
$ node index.js defaultcsv #Para crear un csv por default con los proyectos (tarea). Se guarda en 'csv/default.csv'
$ cp csv/default.csv csv/carga.csv
```

#### 3.
    Modificar el csv/carga.csv y llenar los siguientes campos
        - subject #Asunto de la tarea
        - description #Descripción de la tarea
        - month #Año mes de la tarea en el formato yyyy-mm (Ej: 2017-01)
        - tiempo_dedicado #Las horas dedicadas
        - custom_field_values_25 #Por default está en 'Desarrollo', se puede utilizar otras como 'Análisis', 'Reunión', etc
        
#### 4.
```sh
$ npm index.js #Con esto se cargan las tareas del csv/carga.csv. Se saltean automáticamente las que tengan vacíos algunos de los campos: subject, description, month, tiempo_dedicado
```
    
