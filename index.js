var request = require("request");
var Promise = require('promise');
var async = require("async");
var csv = require('fast-csv');
var fs = require('fs');
var path = require('path');
var config = require(path.join(__dirname, "config", "config"));

var baseRequest = request.defaults({
	baseUrl: 'http://desa.gcba.gob.ar/redmine',
	headers: 
		{ 'cache-control': 'no-cache',
		//'x-redmine-api-key': 'c4f8e3c2d0ef2146c68c9d014d8d20e34b2ab455',
		'x-redmine-api-key': config.key,
		'content-type': 'application/json' 
		},
	json: true
})

var myArgs = process.argv.slice(2);

switch (myArgs[0]) {
    case 'defaultcsv':
        defaultCSV();
    	break;
    case 'tareaspadre':
    	createTareasPadre();
    	break;
    default:
    	loadCSV();
     	break;
}

function loadCSV() {
	csv
	 .fromPath(path.join(__dirname, 'csv','carga.csv'), {headers: true})
	 .on("data", function(data){
	 	var invalid = data.subject == ""
	 	invalid |= data.description == ""
	 	invalid |= data.tiempo_dedicado == ""
	 	invalid |= data.month == ""

	 	if(invalid) {
	 		console.log("Skipping: "+ data.project_name + "-" + data.parent_issue_name)
	 		return;
	 	}
	 	console.log("Saving: "+ data.project_name + "-" + data.parent_issue_name)

	    var date = new Date(data.month);
		var firstDay = new Date(date.getFullYear(), date.getMonth() + 1, 1);
		firstDay = firstDay.getFullYear() + '-' +("0" + (firstDay.getMonth() + 1)).slice(-2) +'-'+("0" + firstDay.getDate()).slice(-2);
		var lastDay = new Date(date.getFullYear(), date.getMonth() + 2, 0);
		lastDay = lastDay.getFullYear() + '-' +("0" + (lastDay.getMonth() + 1)).slice(-2) +'-'+("0" + lastDay.getDate()).slice(-2);

	     var issue = 
			{ issue: 
				{ 	
					project_id: data.project_id,
					tracker_id: data.tracker_id,
					parent_issue_id: data.parent_issue_id,
					subject: data.subject,
					description: data.description,
					status_id: data.status_id,
					priority_id: data.priority_id,
					assigned_to_id: data.assigned_to_id,
					done_ratio: data.done_ratio,
					estimated_hours: data.tiempo_dedicado,
					start_date: firstDay,//Fecha de inicio
					due_date: lastDay,//Fecha de fin
					fixed_version_id: "0", //Version prevista (?)
					custom_fields:
					      [
					        {"value":lastDay,"id":5}, //Fecha fin real
					        {"value":data.tiempo_dedicado,"id":24}, //Tiempo dedicado real
					        {"value":data.custom_field_values_25,"id":25} //Tipo Tarea
					      ]
				} 
			}
		createIssue(issue, null)

	 })
	 .on("end", function(){
	     //console.log("El CSV se ha terminado de procesar.");
	 });
}

function defaultCSV() {
	var data = {};

	getCurrentUser()
	.then(result => { 
	    data.user = result;
	    return getCurrentUserProjectsTareas();
   	})
   	.then(result => {
   		data.projects = result;
   		return getTareasPadre(data.projects);
   	})
   	.then(result => {
   		for(let project_id in data.projects) {
   			data.projects[project_id].tareas_padre = result[project_id];
   		}
   	})
   	.done(() => {
   		createDefaultCSV(data);
   	});
	
}

function getCurrentUser() {
	var promise = new Promise(function (resolve, reject) {
		baseRequest({url: 'users/current.json'}, function(error, response, body){
			resolve(body.user);
		})
  	});

  	return promise;
}

function getCurrentUserProjectsTareas() {
	var promise = new Promise(function (resolve, reject) {
		baseRequest({url: 'projects.json?limit=500'}, function(error, response, body){
			let projects = {};
			for (let project of body.projects) {
				if(project.name.match(/tarea/i)) {
					projects[project.id] = project;
				}
			}
			resolve(projects);
		})
	});

	return promise
}

function getCurrentUserProjectsRequerimientos() {
	var promise = new Promise(function (resolve, reject) {
		baseRequest({url: 'projects.json?limit=500'}, function(error, response, body){
			let projects = {};
			for (let project of body.projects) {
				if(project.name.match(/requerimiento/i)) {
					projects[project.id] = project;
				}
			}
			resolve(projects);
		})
	});

	return promise
}

function getTareasPadre(projects) {
	var promise = new Promise(function (resolve, reject) {
		let issues = {};
		async.each(projects, function(project, callback){
			issues[project.id] = [];
			if(project.parent != undefined) {
				baseRequest({url: `issues.json?project_id=${project.parent.id}&assigned_to_id=116&created_on=>=2016-12-06&limit=500`}, function(error, response, body){
					for (let issue of body.issues) {
						issues[project.id].push(issue);
					}
					callback();
				})
			} else {
				callback();
			}
		}, function(err) {
		    if( err ) {
				console.log('There was an error: ' + err);
		    } else {
				resolve(issues)
		    }
		});
	});

	return promise
}

function createDefaultCSV(data) {
	var csvStream = csv.createWriteStream({headers: true}),
    writableStream = fs.createWriteStream(path.join(__dirname, 'csv','default.csv'));

    writableStream.on("finish", function(){
	  console.log("CSV DONE!");
	});

	csvStream.pipe(writableStream);


	for(let project_id in data.projects) {
		for (let tarea_padre of data.projects[project_id].tareas_padre) {
			csvStream.write({
				project_name : data.projects[project_id].name, 
				project_id: data.projects[project_id].id,
				parent_issue_id: tarea_padre.id,
				parent_issue_name: tarea_padre.subject,
				tracker_id: 2, 
				subject: "", 
				description: "",
				status_id: 3,
				priority_id: 4,
				assigned_to_id: data.user.id,
				custom_field_values_25: "Desarrollo",
				done_ratio: 100,
				month: "",
				tiempo_dedicado: ""
			});
		}
	}
	csvStream.end();
}

var idsProyectosRequerimientos = 
	[
		//193, // Sensores (requerimientos
		//248, // Mobile de Beneficios Tarjeta VOS (Requerimientos)
		//89,  // Academias de Conducir (Requerimientos)
		//220, // Dotaciones de RRHH (Requerimientos)
		//137, // Evaluacion de Desempeño (Requerimientos)
		//107, // Relevamiento de Funciones (Requerimientos)
		//110, // SIFOC Sistema Fondo Compensador (Requerimientos)
		//155, // SiGeCi / Mantenimiento (Requerimientos)
		//203, // Tarjeta Mayor (requerimientos)
		//270, // Encuesta de Urbanización (Requerimientos)
		//289, // Promoción Horizontal (Requerimientos)
		//226, // TARJETA VOS PROYECTOS (REQUERIMIENTOS)
		//236, // Middelware RRHH (Requerimientos)
		//260,  // Portal Participación Ciudadana  (Requerimientos)
		//148,	// Sistema Suma Verde (Requerimientos)
		//150,	// Sistema QR (Requerimientos)
		//152,	// Sistema SubtePass (Requerimientos)
		//158,	//Elecciones 2015 (Requerimientos)
		//160,	//Sistema de Dotaciones (Requerimientos)
		//181,  //SOS Planeta (Requerimiento)
	]
var idsProyectosTareas = 
	[
		196, // Sensores (tareas)
		249, // Mobile de Beneficios Tarjeta VOS (Tareas)
		225, // Tarjeta Vos (tareas)
		206, // Carteleras Digitales (Tareas)
		219, // Dotaciones de RRHH (Tareas)
		138, // Evaluacion de Desempeño (Tareas)
		112, // Relevamiento de Funciones (Tareas)
		132, // SIFOC Sistema Fondo Compensador (Tareas)
		156, // SiGeCi / Mantenimiento (Tareas)
		207, // Tarjeta Mayor (tareas)
		271, // Encuesta de Urbanización (Tareas)
		290, // Promoción Horizontal (Tareas)
		227, // TARJETA VOS PROYECTOS (TAREAS)
		237, // Middelware RRHH (Tareas)
		261  // Portal Participación Ciudadana  (Tareas)
	]

var requerimientos = {
		'Desarrollo - Integración': {
			'subject': 'Desarrollo - Integración',
			'description': 'Contiene las diferentes tareas de desarrollo e integración para el proyecto.'
		},
		'Arquitectura - Modelado': {
			'subject': 'Arquitectura - Modelado',
			'description': 'Contiene las diferentes tareas de arquitectura y modelado para el proyecto.'
		},
		'Análisis': {
			'subject': 'Análisis',
			'description': 'Contiene las diferentes tareas de análsis para el proyecto.'
		},
		'Seguimiento': {
			'subject': 'Seguimiento',
			'description': 'Contiene las diferentes tareas de seguimiento para el proyecto.'
		},
		'Testing': {
			'subject': 'Testing',
			'description': 'Contiene las diferentes tareas de testing para el proyecto.'
		}
	};








/* CREAR TAREAS PADRE */

//createTareasPadre()
function createTareasPadre() {

	getCurrentUserProjectsRequerimientos()
	.then(result => {
		return detectNotCreatedRequriements(result);
	})
	.then(result => {

		var promise = new Promise(function (resolve, reject) {

			async.each(result, function(project, callback) {
				async.forEachOf(project.tareas_padre, function(key, tarea, callback2){
					if(!key) {
						var issue = 
						{ issue: 
							{ 	
								project_id: project.id,
								subject: requerimientos[tarea].subject,
								description: requerimientos[tarea].description,
								status_id: 16,
								priority_id: 4,
								assigned_to_id: 116,
								done_ratio: 0
							} 
						}
						createIssue(issue, callback2)
					} else {
						callback2()
					}
				}, function(err){
					callback()
				});
			}, function(err) {
				if( err ) {
					console.log('There was an error: ' + err);
			    }
			    resolve();
			})
		});

		return promise;

	})
	.done(() => {
   		console.log("Creación de tareas padre finalizada.");
   	});
}

function detectNotCreatedRequriements(projects) {
	var promise = new Promise(function (resolve, reject) {
		
		let requerimientos_keys = Object.keys(requerimientos);
		let requerimientos_creados = {};
		let requerimientos_creados_template = {
			id: "",
			nombre: "",
			tareas_padre: {
				'Desarrollo - Integración': false,
				'Arquitectura - Modelado': false,
				'Análisis': false,
				'Seguimiento': false,
				'Testing': false
			}
		};

		async.each(projects, function(project, callback){
			requerimientos_creados[project.id] = JSON.parse(JSON.stringify(requerimientos_creados_template));
			requerimientos_creados[project.id]['id'] = project.id;
			requerimientos_creados[project.id]['nombre'] = project.name;
			baseRequest({url: `issues.json?project_id=${project.id}&assigned_to_id=116&created_on=>=2016-12-06&limit=500`}, function(error, response, body){
				for (let myissue of body.issues) {
					if(requerimientos_keys.indexOf(myissue.subject) != -1) {
						requerimientos_creados[project.id]["tareas_padre"][myissue.subject] = requerimientos_keys.indexOf(myissue.subject) != -1;
					}

				}
				callback()
			})
		}, function(err) {
		    if( err ) {
				console.log('There was an error: ' + err);
		    } else {
				resolve(requerimientos_creados)
		    }
		});
	});

	return promise
}


function createIssue(issue, callback) {
	var options = { 
	  method: 'POST',
	  url: '/issues.json',
	  body: issue
	}

	baseRequest(options, function (error, response, body) {
	  if (error) throw new Error(error);

	  console.log(body);
	  if (callback != null) {
	  	callback();
	  }
	});
}


/*for (let idProyectoReq of idsProyectosRequerimientos) {
	
	for (let req of requerimientos) {
		var issue = 
			{ issue: 
				{ 	
					project_id: idProyectoReq,
					subject: req.subject,
					description: req.description,
					status_id: 16,
					priority_id: 4,
					assigned_to_id: 116,
					done_ratio: 0
				} 
			}
		createIssue(issue)
	}

}*/